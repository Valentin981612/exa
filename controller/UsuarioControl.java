package com.example.grupoSalinas.controller;

import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.grupoSalinas.app.entity.Usuarios;
import com.example.grupoSalinas.service.UsuarioServicio;
import com.fasterxml.jackson.databind.util.JSONPObject;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioControl {
	
	@Autowired
	private UsuarioServicio usuarioServicio;
	private JSONArray objeson;
	

	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Usuarios usuarios) {
		return ResponseEntity.status(HttpStatus.CREATED).body(usuarioServicio.save(usuarios));
	}
	@GetMapping("/{id}")
	public ResponseEntity<?> read(@PathVariable(value ="id") Long id){
			Optional<Usuarios> oUsuario = usuarioServicio.findById(id);
			System.out.println("oUsuario");
			if(!oUsuario.isPresent()) {
				return ResponseEntity.notFound().build();
			}
			return ResponseEntity.ok(oUsuario);
		}
	
	@PutMapping("/{id}") 
	 public ResponseEntity<?> update(@RequestBody Usuarios usuarioDetalles, @PathVariable(value = "id") Long id){ 
	  Optional<Usuarios> oUsuario =  usuarioServicio.findById(id); 
	  if(!oUsuario.isPresent()) { 
	   return ResponseEntity.notFound().build(); 
	  } 
	  oUsuario.get().setNombre(usuarioDetalles.getNombre()); 
	  oUsuario.get().setApelldio(usuarioDetalles.getApelldio()); 
	  oUsuario.get().setEmail(usuarioDetalles.getEmail()); 
	  oUsuario.get().setHabilitado(usuarioDetalles.isHabilitado()); 
	   
	  return  ResponseEntity.status(HttpStatus.CREATED).body(usuarioServicio.save(oUsuario.get())); 
	   
	 } 
	  
	 @DeleteMapping("/{id}") 
	 public ResponseEntity<?> delate(@PathVariable(value = "id") Long id){ 
	  if(!usuarioServicio.findById(id).isPresent()) { 
	    return ResponseEntity.notFound().build();  
	    
	  } 
	  usuarioServicio.DelateById(id);
	  return ResponseEntity.ok().build(); 
	 } 
	 
	 @GetMapping
	 public ResponseEntity<?>readall(){
		 
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(usuarioServicio.findAll());
		 
	 }
	 
	 
	 @RequestMapping(value = "/obtenerCuentaUsuario", method = RequestMethod.GET	, produces = "aplication/json; charset-usft-8")
	 
	 public @ResponseBody String obtenerCuentaUsuario() {
		 
		 String respuesta = "";
		 JSONObject obJson = new JSONObject();	 
		 JSONArray myArray = new JSONArray();
		 try {
			 System.out.println(":" + respuesta);
			 
			respuesta = usuarioServicio.cuentaUsuarios();
			 System.out.println(":" + respuesta);

			 obJson.put("respuesta","ok");
			 obJson.put("data",respuesta);
			 System.out.println(respuesta);
				
			} catch (Exception e) {
				obJson.put("respuesta","Error");
				obJson.put("data","null");
	   	}
		 myArray.put(obJson);
		 return myArray.toString();
			 
	}
	 
 @RequestMapping(value = "/createUsuario", method = RequestMethod.POST, produces = "aplication/json; charset-usft-8")
	 
	 public @ResponseBody String obtenerCreateUsuario() {
		 
		 String respuesta = "body de ejemplo";
		 JSONObject obJson = new JSONObject();	 
		 JSONArray myArray = new JSONArray();
		 try {
			 System.out.println(":" + respuesta);  usuarioServicio.ceateUsuarios();
			 System.out.println(":" + respuesta);

			 obJson.put("respuesta","ok");
			 obJson.put("data",respuesta);
			 System.out.println(respuesta);
				
			} catch (Exception e) {
				obJson.put("respuesta","Error");
				obJson.put("data","null");
	   	}
		 myArray.put(obJson);
		 return myArray.toString();
			 
	}
 
 
 
 @RequestMapping(value = "/agregar", method = RequestMethod.POST, produces = "aplication/json; charset-usft-8")
 
 public @ResponseBody String agregar(@RequestBody String usuario) {
	 

	 System.out.println(usuario);
	 String respuesta = "";
	 
	 JSONObject objson = new JSONObject(usuario);
	 
	 
	 String nombre = (String) objson.get("nombre");
	 
	 String apellido = (String) objson.get("apellido");
	 
	 String email = (String) objson.get("email");
	 Boolean habilitado = (Boolean) objson.get("habilitado");
	 
	 
	 JSONArray myArray = new JSONArray();
	 
	 try {
		 System.out.println("entro aqui 1 try");
		 respuesta = usuarioServicio.agregar(nombre, apellido, email, habilitado);
		 objson.put("respuesta", "Usuario registrado");
		 objson.put("data", respuesta);
		 System.out.print("listo");
		 
		 
		 
	} catch (Exception e) {
		objson.put("resouesta","error");
		objson.put("data", "null");
	
	}
	 myArray.put(objson);
	 return myArray.toString();
	 
 }
 
@RequestMapping(value = "/editar/{id}", method = RequestMethod.PUT, produces = "application/json; charset-usft-8")
 @ResponseBody
 public  String editar(@RequestBody String usuario, @PathVariable(value = "id")Long id)  {


	 System.out.println(usuario);
	 String respuesta = "";
	 
	 JSONObject objson = new JSONObject(usuario);
	 String nombre = (String) objson.get("nombre");
	 String apellido = (String) objson.get("apellido");
	 String email1 = (String) objson.get("email");
	 Boolean habilitado = (Boolean) objson.get("habilitado");
	 
	 
	 
	 JSONArray myArray = new JSONArray();
	 
	 try {
		 System.out.println("entro aqui ");
		 respuesta = usuarioServicio.editar(  nombre, apellido, email1, habilitado, id );
		 objson.put("respuesta", "Usuario registrado");
		 objson.put("data", respuesta);
		 System.out.print("listo");
		 
		 
	} catch (Exception e) {

		objson.put("respuesta","error");
		objson.put("data", "null");
	
	}
	 myArray.put(objson);
	 return myArray.toString();
 }
@RequestMapping(value ="/eliminar/{id}", method = RequestMethod.DELETE, produces="application/json; charset-usft-8")
@ResponseBody
public String eliminar(@PathVariable(value="id") Long id) {
	String respuesta;
	
	try {
		System.out.println("entro");
		respuesta = usuarioServicio.eliminar(id);
		System.out.println("exitoso");
				
	} catch (Exception e) {
		respuesta="Error";
	}
	return respuesta;
	
	
}
		




}

		 
		



































