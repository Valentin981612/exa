package com.example.grupoSalinas.controller;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.grupoSalinas.app.entity.Animales;
import com.example.grupoSalinas.service.AnimalesServicio;

@RestController
@RequestMapping("api/animales")
public class AnimalesController {
	
	@Autowired private AnimalesServicio animalesServicio;
	
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Animales animales){
		return ResponseEntity.status(HttpStatus.CREATED).body(animalesServicio.save(animales));
	}
	@PostMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value="id") Long id){
		Optional<Animales>oAnimales = animalesServicio.findById(id);
		if(!oAnimales.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oAnimales);
	}
	
	@PutMapping("/{id}") 
	 public ResponseEntity<?> update(@RequestBody Animales animalesDetalles, @PathVariable(value = "id") Long id){ 
	  Optional<Animales> oAnimales =  animalesServicio.findById(id); 
	  if(!oAnimales.isPresent()) { 
	   return ResponseEntity.notFound().build(); 
	  } 
	  oAnimales.get().setNombre(animalesDetalles.getNombre()); 
	  oAnimales.get().setAlimentacion(animalesDetalles.getAllimentacion()); 
	  oAnimales.get().setColor(animalesDetalles.getColor()); 
	  oAnimales.get().setHabilitado(animalesDetalles.isHabilitado()); 
	   
	  return  ResponseEntity.status(HttpStatus.CREATED).body(animalesServicio.save(oAnimales.get())); 
	   
	 } 
	
	@DeleteMapping("/{id}")
	ResponseEntity<?>delate(@PathVariable(value  ="id") Long id){
		if(!animalesServicio.findById(id).isPresent()) {
			return ResponseEntity.notFound().build();
		}
		animalesServicio.DelateById(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping
	public ResponseEntity<?>redall(){
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(animalesServicio.findAll());
	}
	
	@GetMapping("contar")
	public ResponseEntity<?> totalAnima(){
		int total = 0;
		Iterable<Animales> oAnimales = animalesServicio.findAll();
		for(Object i :oAnimales){
			total++;
		}
		return ResponseEntity.ok("En total son: " + total);
	}
	
	
}
























