package com.example.grupoSalinas.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.grupoSalinas.app.entity.Perro;
import com.example.grupoSalinas.service.PerroServicio;


@RestController
@RequestMapping("api/perro")
public class PerroController {
	
	@Autowired private PerroServicio perroServicio;
	private Optional<Perro> oPerro;
	
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Perro perro){
		return ResponseEntity.status(HttpStatus.CREATED).body(perroServicio.save(perro));
	}
	@PostMapping("/{id}")
	public ResponseEntity<?> read (@PathVariable(value="id") Long id){
		Optional<Perro>oPerro = perroServicio.findById(id);
		if(!oPerro.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oPerro);
	}
	
	@PutMapping("/{id}") 
	 public ResponseEntity<?> update(@RequestBody Perro perroDetalles, @PathVariable(value = "id") Long id){ 
	  Optional<Perro> oPerro = perroServicio.findById(id); 
	  if(!oPerro.isPresent()) { 
	   return ResponseEntity.notFound().build(); 
	  } 
	  oPerro.get().setNombre(perroDetalles.getNombre()); 
	  oPerro.get().setAlimentacion(perroDetalles.getAllimentacion()); 
	  oPerro.get().setColor(perroDetalles.getColor()); 
	  oPerro.get().setHabilitado(perroDetalles.isHabilitado()); 
	   
	  return  ResponseEntity.status(HttpStatus.CREATED).body(perroServicio.save(oPerro.get())); 
	   
	 } 
	
	@DeleteMapping("/{id}")
	ResponseEntity<?>delate(@PathVariable(value  ="id") Long id){
		if(!perroServicio.findById(id).isPresent()) {
			return ResponseEntity.notFound().build();
		}
		perroServicio.DelateById(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping
	public ResponseEntity<?>redall(){
		return ResponseEntity.status(HttpStatus.ACCEPTED).body(perroServicio.findAll());
	}

}

