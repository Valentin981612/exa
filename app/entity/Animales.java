package com.example.grupoSalinas.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "animales")

public class Animales implements Serializable{

	
	private static final long serialVersion = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(length =50)
	private String nombre;
	private String alimentacion;
	private String color;
	private String raza;
	 //@Column(name="correo", nullable = false, length = 50, unique =true)
	private boolean habilitado;
	
		
	public Animales() {
		
	}
	
	public Animales(String nombre2, String alimentacion2, String color2) {
		// TODO Auto-generated constructor stub
		this.nombre = nombre;
		this.alimentacion = alimentacion;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	public String getAllimentacion() {
		return alimentacion;
	}
	public void setAlimentacion(String alimentacion) {
		this.alimentacion = alimentacion;
	}
	
	
	
	public String getColor() {
		return color ;
	}
	public void setColor(String color ) {
		this.color = color;
	}
	
	
	
	public String getRaza() {
		return raza;
	}
	public void setRaza(String raza) {
		this.raza = raza;
	}
	
	
	
	
	
	public boolean isHabilitado() {
		return habilitado;
	}
	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	public static long getSerialversion() {
		return serialVersion;
	}
}
