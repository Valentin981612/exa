package com.example.grupoSalinas.service;
	

	import java.util.Optional;


	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.data.domain.Page;
	import org.springframework.data.domain.Pageable;
	import org.springframework.stereotype.Service;
	import org.springframework.transaction.annotation.Transactional;

import com.example.grupoSalinas.app.entity.Animales;
import com.example.grupoSalinas.repository.AnimalesRepositorio;
	
	
	@Service 

	public class AnimalesServicioImplements implements AnimalesServicio{

		@Autowired
		private AnimalesRepositorio animalesRepositorio;
		@Override
		@Transactional(readOnly =true)
		public Iterable<Animales> findAll() {
			// TODO Auto-generated method stub
			return animalesRepositorio.findAll();
		}

		@Override
		@Transactional(readOnly =true)
		public Page<Animales> finAll(Pageable pegeable) {
			// TODO Auto-generated method stub
			return animalesRepositorio.findAll(pegeable);
		}

		@Override
		@Transactional(readOnly =true)
		public Optional<Animales> findById(Long id) {
			// TODO Auto-generated method stub
			return animalesRepositorio.findById(id);
		}

		@Override
		@Transactional
		public Animales save(Animales animales){
			// TODO Auto-generated method stub
			return animalesRepositorio.save(animales);
		}

		@Override
		@Transactional
		public void deleteById(Long Id) {
			// TODO Auto-generated method stub
			animalesRepositorio.deleteById(Id);
		}

		@Override
		public void DelateById(Long id) {
			// TODO Auto-generated method stub
			animalesRepositorio.deleteById(id);
		}

		
	}
