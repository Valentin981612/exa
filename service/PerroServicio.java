package com.example.grupoSalinas.service;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.grupoSalinas.app.entity.Animales;
import com.example.grupoSalinas.app.entity.Perro;

@Service 

public interface PerroServicio {

	public Iterable<Perro> findAll();
	public Page<Perro> finAll(Pageable pegeable);
	public Optional<Perro> findById(Long id);
	public Perro save(Perro Perro);
	public void deleteById(Long id);
	public void DelateById(Long id); 
			
}
