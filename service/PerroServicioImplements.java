package com.example.grupoSalinas.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.grupoSalinas.app.entity.Animales;
import com.example.grupoSalinas.app.entity.Perro;
import com.example.grupoSalinas.repository.PerroRepositorio;

@Service 

public class PerroServicioImplements implements PerroServicio{

	@Autowired
	private PerroRepositorio perroRepositorio;
	@Override
	@Transactional(readOnly =true)
	public Iterable<Perro> findAll() {
		// TODO Auto-generated method stub
		return perroRepositorio.findAll();
	}

	@Override
	@Transactional(readOnly =true)
	public Page<Perro> finAll(Pageable pegeable) {
		// TODO Auto-generated method stub
		return perroRepositorio.findAll(pegeable);
	}

	@Override
	@Transactional(readOnly =true)
	public Optional<Perro> findById(Long id) {
		// TODO Auto-generated method stub
		return perroRepositorio.findById(id);
	}

	@Override
	@Transactional
	public Perro save(Perro perro){
		// TODO Auto-generated method stub
		return perroRepositorio.save(perro);
	}

	@Override
	@Transactional
	public void deleteById(Long Id) {
		// TODO Auto-generated method stub
		perroRepositorio.deleteById(Id);
	}

	@Override
	public void DelateById(Long id) {
		// TODO Auto-generated method stub
		perroRepositorio.deleteById(id);
	}

	
	}

	

