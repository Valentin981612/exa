package com.example.grupoSalinas.service;

	import java.util.Optional;

	import org.springframework.data.domain.Page;
	import org.springframework.data.domain.Pageable;
	import org.springframework.stereotype.Service;

import com.example.grupoSalinas.app.entity.Animales;

	@Service 

	public interface AnimalesServicio {

		public Iterable<Animales> findAll();
		public Page<Animales> finAll(Pageable pegeable);
		public Optional<Animales> findById(Long id);
		public Animales save(Animales animales);
		public void deleteById(Long id);
		public void DelateById(Long id); 
				
}

