package com.example.grupoSalinas.service;

import java.util.Optional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.grupoSalinas.app.entity.Usuarios;
import com.example.grupoSalinas.repository.UsuarioRepositorio;

import org.hibernate.Session;
import org.hibernate.Transaction;

@Service 

public class UsuariosServicioImplements implements UsuarioServicio{

	
	
	@Autowired
	private UsuarioRepositorio usuarioRepositorio;
	
	@Autowired
	private SessionFactory  sessionFactory;

	
	@Override
	@Transactional(readOnly =true)
	public Iterable<Usuarios> findAll() {
		return usuarioRepositorio.findAll();
	}

	@Transactional(readOnly =true)

	public Page<Usuarios> finAll(Pageable pegeable) {
		// TODO Auto-generated method stub
		return usuarioRepositorio.findAll(pegeable);
	}

	@Override
	@Transactional(readOnly =true)

	public Optional<Usuarios> findById(Long id) {
		// TODO Auto-generated method stub
		return usuarioRepositorio.findById(id);
	}

	@Override
	@Transactional
	public Usuarios save(Usuarios usuarios) {
		// TODO Auto-generated method stub
		return usuarioRepositorio.save(usuarios);
	}

	@Override
	@Transactional
	public void DelateById(Long id) {
		// TODO Auto-generated method stub
		usuarioRepositorio.deleteById( id);
	}

	
	

	@Override 
	public String cuentaUsuarios() {
		
		final String consulta = "SELECT COUNT(*) FROM db_zoologico.usuarios";
		String respuesta ="";

		try {
			
			Session sesion = sessionFactory.openSession();

			Query query = sesion.createSQLQuery(consulta);

			respuesta = query.uniqueResult().toString();
			
			System.out.println("Esta es la consulta" + respuesta);

		} catch (Exception e) {
			System.out.println("esta es la consulta" + e);
			respuesta = "Error fatal";
			
		}
	return respuesta;
		 
	}	

	@Override
	public Page<Usuarios> finAll(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String cuentaUsuarios1() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	@Override
	@Transactional 
	public String agregar(String nombre,String apellido, String email, Boolean habilitado) {
		System.out.print("datos:"+"nombre"+nombre+"apellido"+apellido+"email"+email+"habilitado"+habilitado);
		final String consulta ="INSERT INTO usuarios(apelldio,correo,habilitado,nombre)"
				+ "VALUES(?1,?2,?3,?4);";
		String respuesta ="";
		
		try {
			Session session = null;
			
			try {
				session = sessionFactory.openSession();
				session.beginTransaction();
				Query query = session.createSQLQuery(consulta);
	
				query.setParameter(1, apellido);
				query.setParameter(2, email);
				query.setParameter(3, habilitado);
				query.setParameter(4, nombre);
				query.executeUpdate();
				session.getTransaction().commit();
				System.out.println();
				
				respuesta ="Usuario registrado";
				session.close();
			} catch (Exception e) {
				System.out.println();
				respuesta ="error";
			}
		} catch (Exception e) {
			System.out.println("sesion no iniciada" + e);
		}
		return respuesta;
		
	}

	@Override
	public String contarUsuarios() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String ceateUsuarios() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String crearUsuarios() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Transactional 
	public String editar(String nombre,String apellido, String email, Boolean habilitado, Long id) {
		
		final String consulta ="Update usuarios SET nombre = ?1, apelldio = ?2, habilitado =?3, correo = ?4 WHERE id="+id;
		String respuesta ="";
		
		try {
			Session session = null;
			
			try {
				session = sessionFactory.openSession();
				session.beginTransaction();
				Query query = session.createSQLQuery(consulta);
				query.setParameter(1, apellido);
				query.setParameter(2, email);
				query.setParameter(3, habilitado);
				query.setParameter(4, nombre);
				query.executeUpdate();
				session.getTransaction().commit();
				System.out.println("antes");
				
				respuesta ="Usuario modificado";
				session.close();
			} catch (Exception e) {
				System.out.println("error "+ e);
				respuesta ="error";
			}
		} catch (Exception e) {
			System.out.println("sesion no iniciada" + e);
		}
		return respuesta;
		
	}

	
	@Override 
	@Transactional
	public String eliminar (Long id) {
		final String consulta= "DELETE FROM usuarios WHERE id ="+id+"";
		String respuesta ="";
		
		try {
			Session session = null;
			try {
				session = sessionFactory.openSession();
				session.beginTransaction();
				Query query = session.createSQLQuery(consulta);
				query.executeUpdate();
				session.getTransaction().commit();
				System.out.println("antes");
				
				respuesta ="usuario Eliminado";
				session.close();
			} catch (Exception e) {
				System.out.println("error "+ e);
				respuesta ="error";			}
			
			
		} catch (Exception e) {
			System.out.println("sesion no iniciada" + e);
		}
		return respuesta;
			
			
	}				
}	
	
	
	

























