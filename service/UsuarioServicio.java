package com.example.grupoSalinas.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.grupoSalinas.app.entity.Usuarios;

@Service 

public interface UsuarioServicio {

	public Iterable<Usuarios> findAll();
	public Page<Usuarios> finAll(Long id);
	public Optional<Usuarios> findById(Long id);
	public Usuarios save(Usuarios usuarios);
	public void DelateById(Long id);
	public String contarUsuarios();
	public String cuentaUsuarios();
	String cuentaUsuarios1();
	String ceateUsuarios();
	String crearUsuarios();
	public String editar(String nombre2, String nombre, String apellido, Boolean habilitado, Long id);
	public String agregar(String nombre, String apellido, String email, Boolean habilitado);
	String eliminar(Long id);

	
	
	
}  












