package com.example.grupoSalinas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.grupoSalinas.app.entity.Animales;

@Repository
public interface AnimalesRepositorio extends JpaRepository<Animales, Long> {

}
