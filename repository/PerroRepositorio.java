package com.example.grupoSalinas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.grupoSalinas.app.entity.Perro;



@Repository
public interface PerroRepositorio extends JpaRepository<Perro, Long> {

}

